指标
===

# 1. 通达信指标迁移指南
## 1.1. 逻辑运算, 指标迁移中pandas不能使用python的逻辑(位)运算符, 否则会产生歧义

| 逻辑 | 通达信 | python |
|:---|:----|:-------|
| 与 | AND | &      |
| 或 | OR  | \| |
| 非 | NOT | ~      |

## 1.2. 逻辑运算符左右两侧非单一series的必须用()
